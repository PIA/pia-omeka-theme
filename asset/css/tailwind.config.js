/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./../../view/**/*.{phtml,js}",
    "./../../../../modules/ParticipatoryArchives/view/**/*.{phtml,js}"
  ],
  theme: {
    extend: {},
  },
  plugins: [
    'postcss-import'
  ],
}
